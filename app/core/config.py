from typing import Any, Dict, Optional

from pydantic import AnyUrl, BaseSettings, validator


class Settings(BaseSettings):
    API_V1_STR: str = "/api/v1"
    PROJECT_NAME: str = "Geocoding"

    DB_SCHEME: str = "sqlite"
    DB_SERVER: Optional[str] = ""
    DB_PORT: Optional[str]
    DB_USER: Optional[str]
    DB_PASSWORD: Optional[str]
    DB_DB: Optional[str] = "test.db"
    SQLALCHEMY_DATABASE_URI: Optional[str] = None

    # pylint: disable=no-self-argument
    @validator("SQLALCHEMY_DATABASE_URI", pre=True)
    def assemble_db_connection(cls, value: Optional[str], values: Dict[str, Any]) -> Any:
        if isinstance(value, str):
            return value
        return AnyUrl.build(
            scheme=values.get("DB_SCHEME"),
            user=values.get("DB_USER"),
            password=values.get("DB_PASSWORD"),
            host=values.get("DB_SERVER"),
            port=values.get("DB_PORT"),
            path=f"/{values.get('DB_DB') or ''}",
        )


settings = Settings()
