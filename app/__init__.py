# -*- coding: utf-8 -*-

"""Top-level package for Carculator Real-Time Matching."""

__author__ = """Veronika Haaf"""
__email__ = 'vh@cognify.ai'
__version__ = '__version__ = "0.0.10"'
