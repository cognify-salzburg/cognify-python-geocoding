import json
import logging

import uvicorn
from fastapi import FastAPI
from sqlalchemy import create_engine
from uvicorn.config import LOG_LEVELS

# important to make Base.metadata.create_all create the test-table, otherwise it would not know about it
# noinspection PyUnresolvedReferences
import repository.entity.test
from api.api_v1.api import api_router
from core.config import settings
from repository.entity.base_entity import Base

app = FastAPI(title=settings.PROJECT_NAME,
              openapi_url=f"{settings.API_V1_STR}/openapi.json",
              docs_url=f"{settings.API_V1_STR}/docs",
              redoc_url=None)
app.include_router(api_router, prefix=settings.API_V1_STR)
Base.metadata.create_all(create_engine(settings.SQLALCHEMY_DATABASE_URI, **{"pool_pre_ping": True}))


@app.on_event("startup")
def startup():
    logging.info("Started %s", settings.PROJECT_NAME)


@app.on_event("shutdown")
def shutdown():
    logging.info("Shutdown %s", settings.PROJECT_NAME)


if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8000, debug=False, http="httptools",
                log_level=LOG_LEVELS["info"],
                log_config=json.load(open("data/logconfig.json")))
