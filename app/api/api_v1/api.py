from fastapi import APIRouter

from api.api_v1.endpoints import geocoding

api_router = APIRouter()
api_router.include_router(geocoding.router, prefix='/geocoding', tags=['Geocoding'])
