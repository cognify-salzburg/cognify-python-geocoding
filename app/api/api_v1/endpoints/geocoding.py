from fastapi import APIRouter, Depends

from api.api_v1.dto.geocoding import GeocodingRequestDTO, GeocodingResponseDTO

router = APIRouter()


@router.get("/geocode")
def geocode(request: GeocodingRequestDTO = Depends()) -> GeocodingResponseDTO:
    """
    Get information of logged in user.
    """
    return GeocodingResponseDTO(latitude=0, longitude=0)
