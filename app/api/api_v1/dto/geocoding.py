from pydantic import BaseModel, Field


class GeocodingRequestDTO(BaseModel):
    address: str = Field(..., description="address to geocode")


class GeocodingResponseDTO(BaseModel):
    latitude: float = Field(..., description="latitude of the address")
    longitude: float = Field(..., description="longitude of the address")
