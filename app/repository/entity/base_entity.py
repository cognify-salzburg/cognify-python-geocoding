# pylint: disable=no-self-argument,no-member

import stringcase
from sqlalchemy.ext.declarative import as_declarative, declared_attr
from sqlalchemy_utils import Timestamp, generic_repr


@as_declarative()
@generic_repr
class Base(Timestamp):
    # Generate __tablename__ automatically
    @declared_attr
    def __tablename__(cls) -> str:
        return stringcase.snakecase(cls.__name__).lstrip("_")
