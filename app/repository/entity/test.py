from sqlalchemy import BigInteger, Column

from repository.entity.base_entity import Base


class Test(Base):
    id = Column(BigInteger, primary_key=True, index=True)
