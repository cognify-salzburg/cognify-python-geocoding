# Geocoding
This project contains a skeleton for a RESTful endpoint for resolving addresses to geo coordinates.

## Where to find what?
```
api/
|-- api_v1/
    |-- dto         predefined DTOs for the API
    |-- endpoints   controller
core/               generic stuff like config classes
data/               necessary resources like logging configuration
repository/         database repositories
|-- entity/         database entities
service/            actual classes that do stuff
main.py             main entrypoint
```

## How to make this run?
1. install requirements in a virtual environment (Python 3.7+)
2. start `main.py`

You should be able to access the docs url via `http://localhost:8000/api/v1/docs`
A sqlite database file should be created with a test table in it.

No environment variables should be required. Have a look at `app/core/config.py` if you want to know what variables can be changed through the environment.

## What is missing?
1. The geocoding endpoint currently returns a dummy response only.  
Please implement a geocoding service that resolves the address to valid geo coordinates.  
Use Google Maps Geocoding API (https://developers.google.com/maps/documentation/geocoding/overview) to fetch the coordinates.

2. Once the resolution works, please implement a database backed caching of the response.  
For this, create an entity and a repository that allows saving and retrieving of cached geocoding responses.  
A test entity is available in `app/repository/entity`, feel free to modify/delete/ignore it.

## How to return the result?
Please create a git bundle (https://git-scm.com/book/de/v2/Git-Tools-Bundling) and return the bundle.
